FROM python:3.8-buster

COPY requirements.txt /usr/local/bin/orion-xandr-proxy-server/requirements.txt
RUN pip3 install -r /usr/local/bin/orion-xandr-proxy-server/requirements.txt

RUN apt-get update && apt-get install -y redis

ADD . /usr/local/bin/orion-xandr-proxy-server/

WORKDIR /usr/local/bin/orion-xandr-proxy-server/

EXPOSE 443

CMD sh /usr/local/bin/orion-xandr-proxy-server/startup.sh