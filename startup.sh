openssl genrsa -out https.key 2048 &&                                                                     \
openssl req -new -key https.key -out https.csr -subj '/C=FR/ST=IDF/L=Paris/CN=www.orion-semantics.com' && \
openssl x509 -req -days 365 -in https.csr -signkey https.key -out https.crt

redis-server --daemonize yes

uwsgi --master --http-timeout 600 --harakiri 600 --https 0.0.0.0:443,https.crt,https.key -w wsgi:app