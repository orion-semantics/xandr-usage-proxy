from datetime import (datetime,
                      timedelta)
import json
import os
from time import (sleep,
                  time)

from Crypto.Hash import SHA384
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
from flask import (Flask,
                   request)
import redis
import requests

SITE_NAME = "https://api.appnexus.com"

app = Flask(__name__)
r = redis.Redis(host="localhost", port=6379, db=0)

# The proxy looks for credentials in environment variables
# in the format XANDR_USERNAME_<SEAT> and XANDR_PASSWORD_<SEAT>:
XANDR_CREDS = {}
for k, v in os.environ.items():
    if k.startswith("XANDR_USERNAME_") and k.replace("XANDR_USERNAME_", "").isdigit():
        XANDR_CREDS.setdefault(int(k.replace("XANDR_USERNAME_", "")), {})
        XANDR_CREDS[int(k.replace("XANDR_USERNAME_", ""))]["username"] = v

    if k.startswith("XANDR_PASSWORD_") and k.replace("XANDR_PASSWORD_", "").isdigit():
        XANDR_CREDS.setdefault(int(k.replace("XANDR_PASSWORD_", "")), {})
        XANDR_CREDS[int(k.replace("XANDR_PASSWORD_", ""))]["password"] = v

if not XANDR_CREDS:
    print("Error - Could not load credentials")
    exit(1)


def check_headers(request):
    timestamp = request.headers.get("timestamp", "0")
    encoded_timestamp = request.headers.get("signed_timestamp", b"")

    try:
        if time() - float(timestamp) >= 10:
            return False
    except:
        return False

    # Check that the signature matches the content submitted:
    public_key = RSA.importKey(open("public_key.der", "rb").read())
    try:
        pkcs1_15.new(public_key).verify(
            SHA384.new(timestamp.encode()), 
            bytes.fromhex(encoded_timestamp)
        )
        return True
    except:
        return False


def get_headers(seat):
    # Check if token exists in the cache:
    token = r.get(f"token_{seat}")
    if token is not None:
        # Use saved token:
        return {"Authorization": token.decode()}

    else:
        # Get new token from AppNexus:
        auth_data = json.dumps(
            {
                "auth": {
                    "username": XANDR_CREDS.get(seat, {}).get("username"),
                    "password": XANDR_CREDS.get(seat, {}).get("password"),
                }
            }
        )

        auth_response = requests.post(
            url=f"{SITE_NAME}/auth",
            data=auth_data
        )

        try:
            assert auth_response.status_code == 200
            token = auth_response.json().get("response").get("token")

            # Store token in cache for 90mn (max lifetime is 120mn):
            r.set(f"token_{seat}", token, ex=60*90)

            headers = {"Authorization": token}
            return headers
        except Exception as e:
            print(e)
            return None


@app.route("/")
@app.route("/healthcheck")
def healthcheck():
    seats = list(XANDR_CREDS.keys())

    invalid_seats = []
    for seat in seats:
        if get_headers(seat) is None:
            invalid_seats.append(seat)

    if invalid_seats:
        invalid_seats_str = ", ".join(
            [
                str(invalid_seat)
                for invalid_seat in invalid_seats
            ]
        )
        return f"ERROR - Credentials are invalid for seat(s): {invalid_seats_str}."

    connected_seats_str = ", ".join(
        [
            str(seat)
            for seat in seats
        ]
    )
    return f"OK -- The proxy is operational; connected to seat(s): {connected_seats_str}."


@app.route("/usage")
def usage():
    try:
        # Stage 1 - Check request authenticity:
        stage = "Request Authenticity"
        if not check_headers(request):
            return "Signature is invalid", 403

        seat = request.args.get("seat")
        if seat is None or isinstance(seat, str) is False or seat.isdigit() is False:
            return "seat parameter is invalid", 403

        seat = int(seat)

        try:
            start_date = request.args.get("start_date")
            start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
        except:
            return "start_date parameter is invalid", 403

        try:
            end_date = request.args.get("end_date")
            end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
        except:
            return "end_date parameter is invalid", 403

        now = datetime.utcnow()

        if now - timedelta(days=45) > start_date:
            return "start_date parameter cannot be older than 45 days", 403

        if start_date > end_date:
            return "end_date parameter cannot be older than start_date parameter", 403

        if end_date > now:
            return "end_date parameter cannot be in the future", 403

        start_date = start_date.strftime("%Y-%m-%d %H:%M:%S")
        end_date = end_date.strftime("%Y-%m-%d %H:%M:%S")

        # Stage 2 - Prepare the authentication header:
        stage = "Xandr token"
        if get_headers(seat) is None:
            return "Could not get a token from AppNexus. Check the credentials.", 403

        # Stage 3 - Request a Buyer Segment Performance report:
        stage = "Report Request"
        report_request = requests.post(
            f"{SITE_NAME}/report",
            headers=get_headers(seat),
            json={
                "report":
                    {
                        "report_type": "buyer_segment_performance",
                        "format": "csv",
                        "start_date": start_date,
                        "end_date": end_date,
                        "orders": [
                            {"order_by": "hour", "direction": "ASC"},
                            {"order_by": "segment_id", "direction": "ASC"},
                        ],
                        "columns": [
                            "hour",
                            "segment_id",
                            "segment_name",
                            "segment_code",
                            "imps",
                            "clicks",
                            "media_cost",
                        ]
                    }
            }
        )

        if report_request.json().get("response", {}).get("status") != "OK":
            return f"Report request status is not OK. \n {report_request.text}", 500

        report_id = report_request.json().get("response", {}).get("report_id")

        if report_id is None:
            return f"Report ID not provided by AppNexus. \n {report_request.text}"

        # Stage 4 - Wait for report to be ready:
        stage = "Report Status"
        trials = 0
        report_ready = False
        while trials < 12 and not report_ready:
            report_status_request = requests.get(
                f"{SITE_NAME}/report?id={report_id}",
                headers=get_headers(seat)
            )
            if report_status_request.json().get("response", {}).get("execution_status") == "ready":
                report_ready = True
            else:
                trials += 1
                sleep(10)

        if not report_ready:
            return "Report Status was not ready after 120 seconds.", 500

        # Stage 5 - Download report:
        stage = "Report Download"
        report_content_request = requests.get(
            f"{SITE_NAME}/report-download?id={report_id}",
            headers=get_headers(seat)
        )

        if report_content_request.status_code != 200:
            return f"report-downlad returned an error code: {report_content_request.status_code}", 500

        # Stage 6 - Only return data about Orion segments:
        else:
            stage = "Return Orion Semantics data"
            lines = report_content_request.text.split("\r\n")
            filtered_lines = [lines[0]] + [
                _line
                for _line in lines[1:]
                if len(_line.split(",")) >= 3 and _line.split(",")[3].lower().startswith("orion")
            ]

            return "\r\n".join(filtered_lines), 200

    except Exception as e:
        return f"Could not retrieve segment usage report, unknown error. Stage: {stage}; Error: {str(e)}", 500


seats_str = ", ".join(
    [
        str(seat)
        for seat in list(XANDR_CREDS.keys())
    ]
)
print(f"Starting service, for seat(s): {seats_str}")
