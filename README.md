# Installation instructions 

## Step 1 : Pull and execute our Docker Image on a Virutal Machine

* Start an instance with a Ubuntu 18 distribution
* Open port 443

## Step 2 : Install our Appnexus Proxy via Docker 

* SSH into the instance.

* Install Docker by running:
```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```

* Once Docker is installed, run the proxy with the following command (replace \<SEAT>, \<USERNAME> and \<PASSWORD> with valid Xandr credentials):
```bash
sudo docker run -e XANDR_USERNAME_<SEAT>=<USERNAME> -e XANDR_PASSWORD_<SEAT>=<PASSWORD> -p 443:443 orionsemantics/xandr-usage-proxy
```

You can specify multiple XANDR_USERNAME_<SEAT> and XANDR_PASSWORD_<SEAT> argument pairs if you wish to configure multiple seats.

## Step 3 : Verify the proxy is functionnal

Run this instruction from your local shell to ensure the proxy is functional: 

```bash
curl -k https://<INSTANCE_IP>:443/healthcheck
```

The proxy should return the following response: 

OK -- The proxy is operational; connected to seat(s): \<SEATS>.
